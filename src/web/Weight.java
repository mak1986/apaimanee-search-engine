package web;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;

public class Weight {
	private ManageDb db;
	static public final double DOC_COUNT = 64;
	public Weight() throws SQLException{
		this.db = new ManageDb();
	}
	static public void main(String[] args) throws SQLException{
		Weight weight = new Weight();
		//weight.calculateIdfWeight();
		//weight.calculateLogTfWeight();
		//weight.calculateTfIdfWeight();
		//weight.calculateN2Norm();
		//weight.lengthNomailization();
	}

	private void calculateIdfWeight() throws SQLException {
		ResultSet resultSet = this.db.query("SELECT * FROM terms;");
		String termId;
		while(resultSet.next()){
			termId =  resultSet.getString("id");
			ResultSet countSet = this.db.query("SELECT COUNT(*) AS c FROM freqs WHERE term_id='"+termId+"';");
			countSet.next();
			double df = Integer.parseInt(countSet.getString("c"));
			double idf = Math.log10(1+DOC_COUNT/df);
			this.db.updateDB("terms", "idf", String.valueOf(idf), "id='"+termId+"'");
			System.out.println(termId+" df: "+df+" idf: "+idf);
		}
	}
	private void calculateLogTfWeight() throws SQLException {
		ResultSet resultSet = this.db.query("SELECT DISTINCT freq FROM freqs;");
		int c = 1;
		while(resultSet.next()){
			int freq = resultSet.getInt("freq");
			double value = Math.log10(1+freq);
			this.db.updateDB("freqs", "log10tf", String.valueOf(value), "freq = '"+String.valueOf(freq)+"'");
			c++;
			System.out.println(c+" : "+value);
		}
	}
	private void calculateTfIdfWeight() throws SQLException{
		ResultSet termsResultSet = this.db.get("terms");
		int c = 1;
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);

		while(termsResultSet.next()){
			double idf = termsResultSet.getDouble("idf");
			String termId = termsResultSet.getString("id");
			ResultSet freqsResultSet = this.db.getWhere("freqs","term_id='"+termId+"';");
			while(freqsResultSet.next()){
				double tfidf = freqsResultSet.getDouble("log10tf")*idf;
				this.db.updateDB("freqs", "tfidf", String.valueOf(tfidf), "term_id='"+termId+"' AND doc_id='"+freqsResultSet.getString("doc_id")+"'");
				System.out.println(df.format(c*100.0/206609)+"% "+c+" : "+ tfidf);c++;
			}
		}
	}
	private void lengthNomailization() throws SQLException{
		ResultSet termsResultSet = this.db.get("terms");
		int c = 1;
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);

		while(termsResultSet.next()){
			double n2Norm = termsResultSet.getDouble("n2norm");
			String termId = termsResultSet.getString("id");
			ResultSet freqsResultSet = this.db.getWhere("freqs","term_id='"+termId+"';");
			while(freqsResultSet.next()){
				double tfidf = freqsResultSet.getDouble("tfidf");
				double lenNorm = tfidf/n2Norm; 
				this.db.updateDB("freqs", "length_normalization", String.valueOf(lenNorm), "term_id='"+termId+"' AND doc_id='"+freqsResultSet.getString("doc_id")+"'");
				System.out.println("lengthNomailization"+df.format(c*100.0/206609)+"% "+c+" : "+ lenNorm);c++;
			}
		}
	}
	private void calculateN2Norm() throws SQLException{
		ResultSet termsResultSet = this.db.get("terms");
		int c = 0;
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		while(termsResultSet.next()){
			String termId = termsResultSet.getString("id");
			ResultSet freqsResultSet = this.db.getWhere("freqs","term_id='"+termId+"';");
			
			double totalSquare = 0;
			while(freqsResultSet.next()){
				double tfidf = freqsResultSet.getDouble("tfidf");
				totalSquare += tfidf*tfidf;
			}
			double n2Norm = Math.sqrt(totalSquare);
			this.db.updateDB("terms", "n2norm", String.valueOf(n2Norm), "id='"+termId+"'");
			System.out.println("calculateN2Norm"+df.format(c*100.0/24289)+"% "+c+" : "+ n2Norm);c++;
		}
	}
}
