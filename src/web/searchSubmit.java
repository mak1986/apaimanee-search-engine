package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class searchSubmit
 */
@WebServlet("/searchSubmit")
public class searchSubmit extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Html html;
	private ManageDb db;
	private Rank rank;

	/**
	 * @throws SQLException 
	 * @see HttpServlet#HttpServlet()
	 */
	public searchSubmit() throws SQLException {
		super();
		db = new ManageDb();
		html = new Html();
		rank = new Rank();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		String queryString = request.getParameter("query");
		String[] ranking = null;
		try {
			ranking = this.rank.getResult(queryString);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		PrintWriter out = response.getWriter();

		String output;
		try {
			output = html.getHeader("Query:"+queryString, "Query:"+queryString) +"<a href='http://localhost:8080/SearchEngine/search'>Back to search</a><br/><br/>" +this.getLinks(ranking) + "</p>" + html.getFooter();
			out.print(output);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	private String getLinks(String[] ranking) throws SQLException{
		String resultString="<ul>";
		for(int i = 0; i<ranking.length;i++){
			ResultSet resultSet = this.db.getWhere("docs","id='"+ranking[i]+"'");
			resultSet.next();
			resultString +="<li>"
					+ "<p><a href='http://localhost:8080/SearchEngine/doc?id="+resultSet.getString("id")+"'>พระอภัยมณีตอนที่ " + resultSet.getString("id") +"</a></p>"
					+ "<p>"+resultSet.getString("content").substring(0, 250)+"...</p><br />"
					+ "</li>";
		}
		resultString +="</ul>";
		return resultString;
	}

}
