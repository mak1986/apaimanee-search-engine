package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class doc
 */
@WebServlet("/doc")
public class doc extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ManageDb db;
	private Html html;
       
    /**
     * @throws SQLException 
     * @see HttpServlet#HttpServlet()
     */
    public doc() throws SQLException {
        super();
		db = new ManageDb();
		html = new Html();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");

		PrintWriter out = response.getWriter();
		String output;
		try {
			output = html.getHeader("พระอภัยมณีตอนที่ "+id, "พระอภัยมณีตอนที่ "+id) + "<p>"
					+ this.getDoc(id) + "</p>" + html.getFooter();
			out.print(output);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	private String getDoc(String id) throws SQLException{
		ResultSet resultSet = this.db.getWhere("docs","id='"+id+"'");
		String resultString="";
		while(resultSet.next()){
			resultString += resultSet.getString("content");
		}
		resultString = resultString.replaceAll("\\r?\\n", "<br />");
		return resultString;
	}

}
