package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class search
 */
@WebServlet("/search")
public class search extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Html html;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public search() {
        super();
        html = new Html();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String output = html.getHeader("ค้นหา", "ค้นหา")
				+ "<form action='http://localhost:8080/SearchEngine/searchSubmit' method='get'>"
				+ "<input id='search-field' type='text' name='query' /><br/><br/>"
				+ "<button class='btn' type='submit'>ค้นหา</button>"
				+ "</form>"
				+ html.getFooter();
		out.println(output);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("test");
		
	}


}
