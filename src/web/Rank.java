package web;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Rank {
	private ManageDb db;
	private ArrayList<String> queryTermIds = new ArrayList<String>();

	public Rank() throws SQLException {
		db = new ManageDb();
	}

	public String[] getResult(String query) throws SQLException {
		ArrayList<String> nGramList = this.queryToNGram2(query);
		ArrayList<ResultSet> resultSets = this.getHighIdf(nGramList);
		this.setQueryTermId(resultSets);
		ArrayList<ResultSet> championLists = this.getChampionLists();
		String[] rank = this.calculateScore(championLists);
		return rank;
	}

	private void setQueryTermId(ArrayList<ResultSet> resultSets)
			throws SQLException {
		for (ResultSet resultSet : resultSets) {
			while (resultSet.next()) {
				String termId = resultSet.getString("id");
				this.queryTermIds.add(termId);
			}
		}
	}

	private String[] calculateScore(ArrayList<ResultSet> championLists)
			throws SQLException {
		HashMap<String, Double> scores = new HashMap<String, Double>();
		for (ResultSet championList : championLists) {
			while (championList.next()) {
				double lenNorm = championList.getDouble("length_normalization");
				String docId = championList.getString("doc_id");
				String termId = championList.getString("term_id");
				if (this.queryTermIds.contains(termId)) {
					if (!scores.containsKey(docId)) {
						scores.put(docId, lenNorm);
					} else {
						scores.put(docId, scores.get(docId) + lenNorm);
					}
				}
			}
		}
		return this.sortChampionList(scores);
	}

	private String[] sortChampionList(HashMap<String, Double> scores) {
		String[] rank = new String[scores.size()];
		List<Entry<String, Double>> sorted = this.sortMapByValues(scores);
		for(int i=0;i<scores.size();i++){
			rank[i]=sorted.get(i).getKey();
		}
		return rank;
	}
	
	public List<Entry<String, Double>> sortMapByValues (HashMap<String,Double> map) {
	    
	    Comparator<Map.Entry<String, Double>> byMapValues = new Comparator<Map.Entry<String, Double>>() {
	        @Override
	        public int compare(Map.Entry<String, Double> left, Map.Entry<String, Double> right) {
	            return right.getValue().compareTo(left.getValue());
	        }
	    };
	    
	    // create a list of map entries
	    List<Map.Entry<String, Double>> sortedList = new ArrayList<Map.Entry<String, Double>>();
	    
	    // add all candy bars
	    sortedList.addAll(map.entrySet());
	    
	    // sort the collection
	    Collections.sort(sortedList, byMapValues);
	    return sortedList;
	    
	}

	private ArrayList<String> queryToNGram2(String query) {
		
		ArrayList<String> nGramList = new ArrayList<String>();
		char[] chars = query.toCharArray();
		for (int i = 0; i < chars.length - 2; i++) {
			nGramList.add(Character.toString(chars[i])
					+ Character.toString(chars[i + 1])
					+ Character.toString(chars[i + 2]));
		}
		return nGramList;
	}

	private ArrayList<ResultSet> getHighIdf(ArrayList<String> nGramList)
			throws SQLException {
		ArrayList<ResultSet> results = new ArrayList<ResultSet>();
		for (String nGram : nGramList) {
			results.add(this.db.getWhere("terms", "term='" + nGram
					+ "' AND idf >= 0.3 ORDER BY idf DESC"));
		}
		return results;
	}

	private ArrayList<ResultSet> getChampionLists() throws SQLException {
		ArrayList<ResultSet> results = new ArrayList<ResultSet>();
		for (String id : this.queryTermIds) {
			results.add(this.db.getWhere("freqs", "term_id='" + id
					+ "' ORDER BY length_normalization DESC LIMIT 20"));
		}
		return results;
	}

	public static void main(String[] args) throws SQLException {
		Rank rank = new Rank();
		String[] ranking = rank.getResult("โศก");
		
		for(int i=0; i<ranking.length;i++){
			System.out.println(ranking[i]);
		}
		
	}

}
