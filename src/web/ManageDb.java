package web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

public class ManageDb {
	Connection connect;
	public ManageDb() throws SQLException{
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.connect = DriverManager.getConnection("jdbc:mysql://localhost/ir?useUnicode=true&characterEncoding=UTF-8&user=root");
		}
	public ResultSet get(String table) throws SQLException{
		Statement statement = connect.createStatement();
		return statement.executeQuery("select * from "+table +";");
	}
	public ResultSet getWhere(String table, String where) throws SQLException{
		Statement statement = connect.createStatement();
		return statement.executeQuery("select * from "+table +" WHERE "+where+";");
	}

	public ResultSet query(String query) throws SQLException{
		Statement statement = connect.createStatement();
		return statement.executeQuery(query);
	}
	public void insertDb(String[] data) throws SQLException{
		Statement statement = connect.createStatement();
		statement.executeQuery("insert into keyword keyword,docId values("+data[0]+","+data[1]+")");
		
	}
	public void deleteDb(String table, String field, String value) throws SQLException{
		Statement statement = connect.createStatement();
		statement.executeQuery("delete from "+ table + " where "+ field + "= '"+ value +"';");
	}
	public void updateDB(String table, String column, String value,String where) throws SQLException{
		Statement statement = connect.createStatement();
		statement.executeUpdate("UPDATE " + table + " SET " + column + "='"+value +"' WHERE " + where +";");
	}
	public ResultSet andSearch(String word1, String word2) throws SQLException{
		Statement statement = connect.createStatement();
		return statement.executeQuery("select distinct a.docId from keyword as a "
				+ "inner join keyword as b on a.docId=b.docId and b.word='"+word1+
				"' inner join keyword as c on a.docId=c.docId and c.word='"+word2+"';");
	}
	public String showResult(ResultSet resultSet) throws SQLException{
		String resultString="";
		while(resultSet.next()){
			resultString +=resultSet.getString("content") + " " +resultSet.getString("id") +"\n";
		}
		return resultString;
	}
}
