package web;

public class Html {
	public String getHeader(String title,String h2){
		String html = "<!DOCTYPE html>"
				+ "<html>"
				+ "<head>"
				+ "<link rel='stylesheet' href='reset.css' type='text/css'/>"
				+ "<link rel='stylesheet' href='style.css' type='text/css'/>"
				+ "<meta charset='UTF-8'/>"
				+ "<title>"+title+"</title>"
				+ "</head>"
				+ "<body>"
				+ "<div id='container'>"
				+ "<h1>พระอภัยมณี SEARCH ENGINE</h1>"
				+ "<br/><br/><h2>"+h2+"</h2><br/>";
		return html;
	}
	public String getFooter(){
		String html = "</div>"
				+ "</body>"
				+ "</html>";
		return html;
	}
	public static void main(String[] args){
	}
}
